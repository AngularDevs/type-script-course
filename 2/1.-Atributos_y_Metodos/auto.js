var Auto = /** @class */ (function () {
    function Auto() {
        this.speed = 0;
    }
    Auto.prototype.getColor = function () {
        return this.color;
    };
    Auto.prototype.setColor = function (color) {
        this.color = color;
    };
    Auto.prototype.speedUp = function () {
        this.speed++;
    };
    Auto.prototype.speedDown = function () {
        this.speed--;
    };
    Auto.prototype.getCurrentSpeed = function () {
        return this.speed;
    };
    return Auto;
}());
var auto = new Auto();
auto.setColor('Azul');
auto.speedUp();
auto.speedUp();
auto.speedUp();
console.log("El color del auto  es: " + auto.getColor());
console.log("La velocidad del auto  es: " + auto.getCurrentSpeed());
auto.speedDown();
console.log("La velocidad del auto  es: " + auto.getCurrentSpeed());
