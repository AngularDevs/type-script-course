class Auto
{
	public color: string;
	public model: string;
	public speed: number = 0;

	public getColor()
	{
		return this.color;
	}
	public setColor(color: string)
	{
		this.color = color;
	}
	public speedUp()
	{
		this.speed++;
	}
	public speedDown()
	{
		this.speed--;
	}
	public getCurrentSpeed(): number
	{
		return this.speed;
	}
}

var auto  = new Auto();
auto.setColor('Azul');
auto.speedUp();
auto.speedUp();
auto.speedUp();

console.log( "El color del auto  es: " + auto.getColor() );
console.log( "La velocidad del auto  es: " + auto.getCurrentSpeed() );

auto.speedDown();
console.log( "La velocidad del auto  es: " + auto.getCurrentSpeed() );
