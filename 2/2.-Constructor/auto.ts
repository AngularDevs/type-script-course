class Auto
{
	public color: string;
	public model: string;
	public speed: number;


	constructor(model:any = null)
	{
		this.speed = 0;
		this.color = "Negro";

		if(model == null)
		{
			this.model = "Sedan";
		}
		else
		{
			this.model = model;
		}
	}

	public getColor()
	{
		return this.color;
	}
	public setColor(color: string)
	{
		this.color = color;
	}
	public getModel()
	{
		return this.model;
	}
	public setModel(model: string)
	{
		this.model = model;
	}

	public speedUp()
	{
		this.speed++;
	}
	public speedDown()
	{
		this.speed--;
	}
	public getCurrentSpeed(): number
	{
		return this.speed;
	}
}

var auto  = new Auto();
//var auto  = new Auto("Deportivo");

auto.setColor('Azul');
auto.speedUp();
auto.speedUp();
auto.speedUp();

console.log( "El color del auto  es: " + auto.getColor() );
console.log( "El model del auto  es: " + auto.getModel() );
console.log( "La velocidad del auto  es: " + auto.getCurrentSpeed() );

auto.speedDown();
console.log( "La velocidad del auto  es: " + auto.getCurrentSpeed() );
