# Programación Orientada a Objectos ( POO )


## Visibilidad

**Public**.- Como su nombre lo indica puede ser accedida desde cualquier parte.

**Protected**.- Se puede acceder de una clase que hereda o desde la misma, pero no desde fuera.

**Private**.- Unicamente visible para la clase donde se declaro
