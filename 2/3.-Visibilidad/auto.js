var Auto = /** @class */ (function () {
    function Auto(model) {
        if (model === void 0) { model = null; }
        this.speed = 0;
        this.color = "Negro";
        if (model == null) {
            this.model = "Sedan";
        }
        else {
            this.model = model;
        }
    }
    Auto.prototype.getColor = function () {
        return this.color;
    };
    Auto.prototype.setColor = function (color) {
        this.color = color;
    };
    Auto.prototype.getModel = function () {
        return this.model;
    };
    Auto.prototype.setModel = function (model) {
        this.model = model;
    };
    Auto.prototype.speedUp = function () {
        this.speed++;
    };
    Auto.prototype.speedDown = function () {
        this.speed--;
    };
    Auto.prototype.getCurrentSpeed = function () {
        return this.speed;
    };
    return Auto;
}());
var auto = new Auto();
//var auto  = new Auto("Deportivo");
auto.setColor('Azul');
auto.speedUp();
auto.speedUp();
auto.speedUp();
console.log("El color del auto  es: " + auto.getColor());
console.log("El model del auto  es: " + auto.getModel());
console.log("La velocidad del auto  es: " + auto.getCurrentSpeed());
auto.speedDown();
console.log("La velocidad del auto  es: " + auto.getCurrentSpeed());
