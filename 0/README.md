# TypeScript

**TypeScript** es un lenguaje de programación libre y de código abierto desarrollado y mantenido por Microsoft. Es un superconjunto de JavaScript, que esencialmente añade tipado estático y objetos basados en clases. Anders Hejlsberg, diseñador de C# y creador de Delphi y Turbo Pascal, ha trabajado en el desarrollo de TypeScript.1​ TypeScript puede ser usado para desarrollar aplicaciones JavaScript que se ejecutarán en el lado del cliente o del servidor (Node.js).

- Nota

Es necesario tener instalado **Node JS** previamente. [Node](https://nodejs.org/es/download/)

## Instalación

Abre la terminal y escribe:

```
npm install -g typescript
```

## Versión de TypeScript

Para revisar la versión que se tiene instalada, abre una terminal y escribe:

```
tsc -v
```

- Documentación

Para mas referencias, puedes revisar la documentación oficial, [Doc](https://www.typescriptlang.org/docs/home.html).
