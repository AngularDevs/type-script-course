# Funciones

De la misma manera en la que podemos especificar, el tipo de variable, también nos es posible especificar el **tipo** de dato que devolverá una función.

La siguiente declaración, tiene como entrada un parámetro, *message* que es el tipo **string**, seguido de ':string', lo que significa que el valor que debe regresar es el tipo string:

```
function printMessage( message: string ):string
{
	...
}
```

El siguiente ejemplo, recive de entra un dato del tipo *number* y de salida devuelve un *string*:

```
function returnNumber(num: number):string
{
	...
}
```

**Codigo completo:**

```
function printMessage( message: string ):string
{
	return "Your message is : " + message;
}

console.log( printMessage("Great job!") );

function returnNumber(num: number):string
{
	return "tú número es: " + num;
}
alert( returnNumber(28) );
```
