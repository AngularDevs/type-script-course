# Introducción

Crea una carpeta con el nombre **0** y dentro crea 2 archivos, **index.html** y **script.ts**

- Index Html

El archivo index, debe contener la estructura base de un proyecto html.

```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>TypeScript</title>
	<script type="text/javascript" src="script.js"></script>
</head>
<body>
	<h1>Course of TypeScript</h1>
	<section id="container">
	</section>
</body>
</html>
```

- Script Ts

Dentro del archivo **script.ts**, declaramos una función con un parámetro de entrada, tal cual se hace en JavaScript.

```
function printMessage( name )
{
	return "Nice to meet you, " + name;
}
```

De bajo de la función declaramos una variable *name* y le asignamos un string con un texto, por ultimo obtenemos el elemento del html *container* para asignarle el texto que regresa **printMessage**.

**Codigo script.ts**

```
function printMessage( name )
{
	return "Nice to meet you, " + name;
}

let user    = "Angular Dev";

// Using a variable - **message**
var message = printMessage( user );
document.getElementById("container").innerHTML = message;

// Same line
//document.getElementById("container").innerHTML = printMessage(user);
```

## De TypeScript a Javascript

Para que el navegador pueda interpretar el archivo, es necesario convertir el archivo de *typescript* a uno de *javascript*. En la terminal, estando en dentro de la carpeta donde se encuentra el archivo ts, teclea tsc seguido del nombre del archivo a convertir:

`tsc script.ts`

## Actividad de Repaso

`Crea una función, que reciba dos parametros, regrese la suma de estos y la muestre en el html`

