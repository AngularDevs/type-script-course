## Solución a la actividad

- Script Ts

Para resolver el problema anterior, agregamos un segundo parámetro a la función separandolos por una **,**.

Podemos hacer uso de una variable auxiliar o hacerlo sobre la misma linea, pero en este caso hay que agregar parentesis.

**Con una variable auxiliar**
```
function printMessage( num1, num2 )
{
	var sum = num1 + num2;
	return "Result: " + sum;
}
```

**Misma Línea**
```
function printMessage( num1, num2 )
{
	return "Result: " + ( num1 + num2 );
}
```
## Nota

Para no estar ejecutando la instruccion ó `tsc script.ts`, podemos usar:
```
tsc -w *.ts
```
Asi cada que se detecte un cambio, en cualquier archivo con la extensión *ts* este sera convertido a un js.

# Variables y tipos

En TypeScript, puedes especificar el tipo de dato.

variable del tipo **string**
```
var user: string = "Angular Dev";
```

variable del tipo **number**
```
var age:  number = 28;
```
variable del tipo **boolean**
```
var developer: boolean = true;
```

Array
```
var things: Array = ["Apple",1,true];
```

Array *tipado*
```
var lang: Array<string> = ["TypeScript","Html","PHP"];
```

## Diferencias entre var y let

La principal diferencia que existe, es el alcance de las variable.

**let** se limita al bloque, declaración o expreción donde se esta utilizando, mientras que **var** define una variable global o local en una función sin importar el ámbito del bloque.

ejemplo:

```
var a = 10;
var b = 12;

if( a === 10 )
{
	let a = 4;
	var b = 1;

	console.log("dentro del if el valor de a es: " + a + " y el valor de b es: "+ b);
}
console.log("fuera del if el valor de a es: " + a + " y el valor de b es: "+ b);
```

