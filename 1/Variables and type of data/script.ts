function printMessage( num1, num2 )
{
	return "Result: " + ( num1 + num2 );
}
// Solution:
document.getElementById("container").innerHTML = printMessage( 10, 15);

// Variable

var user: string = "Angular Dev";
var age:  number = 28;
var developer: boolean = true;

var lang: Array<string> = ["TypeScript","Html","PHP"];

document.getElementById("container").innerHTML = "nombre: " + user + " edad: " + age;

var a = 10;
var b = 12;

if( a === 10 )
{
	let a = 4;
	var b = 1;

	console.log("dentro del if el valor de a es: " + a + " y el valor de b es: "+ b);
}
console.log("fuera del if el valor de a es: " + a + " y el valor de b es: "+ b);

